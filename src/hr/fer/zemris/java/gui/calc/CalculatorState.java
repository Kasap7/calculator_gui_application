package hr.fer.zemris.java.gui.calc;

/**
 * CalculatorState enumeration represents current state of some calculator.
 * 
 * @author Josip Kasap
 *
 */
public enum CalculatorState {
	
	/** This state is activated after some input has been successfully entered. */
	INPUT_ENTERED,
	
	/** This state is activated after some operation has been successfully handled. */
	OPERATION_ENTERED,
	
	/** 
	 * This state is activated after any input that is not valid, for instance: "2 + +"
	 * second + operator is not valid input after one operator has been previously entered.
	 */
	INVALID_INPUT
}
