package hr.fer.zemris.java.gui.calc;

/**
 * This functional interface represents strategy that describes some complex
 * operations with 1 value such as sin, cos, ln...
 * This interface also allows for inverted operations if flag inverse is
 * set to true.
 * 
 * @author Josip Kasap
 *
 */
public interface IOperation {
	
	/**
	 * Method that gets result of operation of some number.
	 * @param number input number of operation
	 * @param inverse should operation be inverse
	 * @return result of operation
	 */
	double getResult(double number, boolean inverse);
}
