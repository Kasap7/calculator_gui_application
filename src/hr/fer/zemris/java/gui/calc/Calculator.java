package hr.fer.zemris.java.gui.calc;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Stack;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import hr.fer.zemris.java.gui.layouts.CalcLayout;
import hr.fer.zemris.java.gui.layouts.RCPosition;

/**
 * Calculator is program that simulates work of simple calculator application.
 * This program uses swing Framework with {@link CalcLayout} LayoutManager.
 * 
 * @author Josip Kasap
 *
 */
public class Calculator extends JFrame {
	
	/** Serial number. */
	private static final long serialVersionUID = 42L;
	
	/** Spacing between 2 components in {@link CalcLayout} manager. */
	public static final int INTERSPACE = 8;
	
	/** Strategy that simulates x + y operation. */
	private static final IBasicOperation ADD_OPERATION = (x1, x2) -> x1 + x2;
	
	/** Strategy that simulates x - y operation. */
	private static final IBasicOperation SUB_OPERATION = (x1, x2) -> x1 - x2;
	
	/** Strategy that simulates x * y operation. */
	private static final IBasicOperation MUL_OPERATION = (x1, x2) -> x1 * x2;
	
	/** Strategy that simulates x / y operation. */
	private static final IBasicOperation DIV_OPERATION = (x1, x2) -> x1 / x2;
	
	/** Strategy that simulates sin(x)|arc sin(x) operation. */
	private static final IOperation SIN_FUNCTION = (num, inv) -> !inv ? Math.sin(num) : Math.asin(num);
	
	/** Strategy that simulates cos(x)|arc cos(x) operation. */
	private static final IOperation COS_FUNCTION = (num, inv) -> !inv ? Math.cos(num) : Math.acos(num);
	
	/** Strategy that simulates tan(x)|arc tan(x) operation. */
	private static final IOperation TAN_FUNCTION = (num, inv) -> !inv ? Math.tan(num) : Math.atan(num);
	
	/** Strategy that simulates ctg(x)|arc ctg(x) operation. */
	private static final IOperation CTG_FUNCTION = (num, inv) -> !inv ? 1/Math.tan(num) : Math.PI/2 - Math.atan(num);
	
	/** Strategy that simulates 1/x operation. */
	private static final IOperation INV_FUNCTION = (num, inv) -> 1/num;
	
	/** Strategy that simulates log(x)|10^x operation. */
	private static final IOperation LOG_FUNCTION = (num, inv) -> !inv ? Math.log10(num) : Math.pow(10, num);
	
	/** Strategy that simulates ln(x)|e^x operation. */
	private static final IOperation LN_FUNCTION = (num, inv) -> !inv ? Math.log(num) : Math.pow(Math.E, num);
	
	/** Label that represents output of calculator. */
	private JLabel resultLabel;
	
	/** Stack of numbers used in push|pop commands. */
	private Stack<Double> numberStack;
	
	/** Check box that represents inverse operations for functions. */
	private JCheckBox invCheckBox;
	
	/** Panel on which all components are placed */
	private JPanel panel;
	
	/** Stored value of calculator. */
	private Double storedValue;
	
	/** Value of current label of calculator */
	private Double labelValue;
	
	/** Stored simple operation of calculator. */
	private IBasicOperation operation;
	
	/** Current state of calculator. */
	private CalculatorState state;
	
	/**
	 * Constructs new calculator.
	 */
	public Calculator() {
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setTitle("Calculator");
		setLocation(20, 20);
		setSize(600, 400);
		numberStack = new Stack<>();
		initGUI();
	}

	/**
	 * Initializes gui.
	 */
	private void initGUI() {
		panel = new JPanel(new CalcLayout(INTERSPACE));
		add(panel);
		
		resultLabel = new JLabel("0");
		resultLabel.setOpaque(true);
		resultLabel.setBackground(Color.YELLOW);
		resultLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		labelValue = 0.0;
		storedValue = 0.0;
		state = CalculatorState.INPUT_ENTERED;
		operation = ADD_OPERATION;
		
		invCheckBox = new JCheckBox("inv", false);
		panel.add(resultLabel, "1,1");
		panel.add(invCheckBox, "5,7");
		
		initStackButtions();
		initBasicOperationsButtons();
		initSpecialButtons();
		initFuncitonButtons();
		initNumberButtons();
	}
	
	/**
	 * Initialized number buttons of calculator.
	 */
	private void initNumberButtons() {
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				RCPosition position = new RCPosition(2 + i, 3 + j);
				int num = 6 + j + 1 - i*3;
				JButton number = new JButton("" + num);
				number.addActionListener(new NumberActionListener(num));
				panel.add(number, position);
			}
		}
		
		JButton zero = new JButton("" + 0);
		zero.addActionListener(new NumberActionListener(0));
		panel.add(zero, "5,3");
	}

	/**
	 * Initialized function buttons of calculator.
	 */
	private void initFuncitonButtons() {
		JButton inverse = new JButton("1/x");
		inverse.addActionListener(new FunctionActionListener(INV_FUNCTION));
		panel.add(inverse, "2,1");
		
		JButton sin = new JButton("sin");
		sin.addActionListener(new FunctionActionListener(SIN_FUNCTION));
		panel.add(sin, "2,2");
		
		JButton cos = new JButton("cos");
		cos.addActionListener(new FunctionActionListener(COS_FUNCTION));
		panel.add(cos, "3,2");
		
		JButton tan = new JButton("tan");
		tan.addActionListener(new FunctionActionListener(TAN_FUNCTION));
		panel.add(tan, "4,2");
		
		JButton ctg = new JButton("ctg");
		ctg.addActionListener(new FunctionActionListener(CTG_FUNCTION));
		panel.add(ctg, "5,2");
		
		JButton log = new JButton("log");
		log.addActionListener(new FunctionActionListener(LOG_FUNCTION));
		panel.add(log, "3,1");
		
		JButton ln = new JButton("ln");
		ln.addActionListener(new FunctionActionListener(LN_FUNCTION));
		panel.add(ln, "4,1");
		
		JButton pow = new JButton("x^n");
		pow.addActionListener((evt) -> {
			if (state != CalculatorState.INPUT_ENTERED) {
				setInvalidInput();
			} else {
				labelValue = operation.getResult(storedValue, labelValue);
				if (labelValue != null && Double.isFinite(labelValue)) {
					storedValue = labelValue;
					operation = (x, n) -> !invCheckBox.isSelected() ? Math.pow(x, n) : Math.pow(x, 1.0/n);
					resultLabel.setText(getTextResult());
					state = CalculatorState.OPERATION_ENTERED;
				} else {
					setInvalidInput();
				}
			}
			panel.updateUI();
		});
		panel.add(pow, "5,1");
	}
	
	/**
	 * FunctionActionListener is {@link ActionListener} for complex function
	 * buttons.
	 * 
	 * @author Josip Kasap
	 *
	 */
	private class FunctionActionListener implements ActionListener {
		
		/** Complex function strategy. */
		private IOperation oper;
		
		/**
		 * Constructs new FunctionActionListener with given complex function strategy.
		 * @param oper complex function strategy
		 */
		private FunctionActionListener(IOperation oper) {
			this.oper = oper;
		}
		
		@Override
		public void actionPerformed(ActionEvent e) {
			if (state == CalculatorState.INPUT_ENTERED) {
				labelValue = oper.getResult(labelValue, invCheckBox.isSelected());
				resultLabel.setText(getTextResult());
				if (labelValue != null && Double.isFinite(labelValue)) {
					state = CalculatorState.INPUT_ENTERED;
				} else {
					setInvalidInput();
				}
			} else {
				setInvalidInput();
			}
			panel.updateUI();
		}
	}
	
	/**
	 * NumberActionListener is {@link ActionListener} for number buttons.
	 * 
	 * @author Josip Kasap
	 *
	 */
	private class NumberActionListener implements ActionListener {
		
		/** Digit of number button. */
		private int number;
		
		/**
		 * Constructs new NumberActionListener with given digit of number button.
		 * @param number digit of number button
		 */
		private NumberActionListener(int number) {
			this.number = number;
		}
		
		@Override
		public void actionPerformed(ActionEvent arg0) {
			if (state == CalculatorState.INPUT_ENTERED) {
				String newResult;
				if (!resultLabel.getText().equals("0")) {
				newResult = resultLabel.getText() + number;
				} else {
					newResult = "" + number;
				}
				labelValue = Double.parseDouble(newResult);
				resultLabel.setText(newResult);
			} else if (state == CalculatorState.OPERATION_ENTERED) {
				labelValue = (double)number;
				resultLabel.setText("" + number);
				state = CalculatorState.INPUT_ENTERED;
			} else {
				setInvalidInput();
			}
		}
	}

	/**
	 * Initialized special buttons of calculator.
	 */
	private void initSpecialButtons() {
		JButton sign = new JButton("+/-");
		sign.addActionListener((evt) -> {
			if (state == CalculatorState.INPUT_ENTERED) {
				labelValue = - labelValue;
				String newResult = resultLabel.getText();
				if (newResult.startsWith("-")) {
					newResult = newResult.substring(1);
				} else {
					newResult = "-" + newResult;
				}
				resultLabel.setText(newResult);
			} else {
				setInvalidInput();
			}
			panel.updateUI();
		});
		panel.add(sign, "5,4");
		
		JButton dot = new JButton(".");
		dot.addActionListener((evt) -> {
			if (state == CalculatorState.INPUT_ENTERED) {
				String newInput = resultLabel.getText() + ".";
				try {
					labelValue = Double.parseDouble(newInput);
					resultLabel.setText(newInput);
				} catch (NumberFormatException e) {
					setInvalidInput();
				}
			} else {
				setInvalidInput();
			}
			panel.updateUI();
		});
		panel.add(dot, "5,5");
	}

	/**
	 * Initialized basic operation buttons of calculator.
	 */
	private void initBasicOperationsButtons() {
		JButton clr = new JButton("clr");
		JButton res = new JButton("res");
		
		clr.addActionListener((evt) -> {
			resultLabel.setText("0");
			labelValue = 0.0;
			state = CalculatorState.INPUT_ENTERED;
			if (operation == null) {
				operation = ADD_OPERATION;
			}
			panel.updateUI();
		});
		panel.add(clr, "1,7");
		
		res.addActionListener((evt) -> {
			resultLabel.setText("0");
			labelValue = 0.0;
			storedValue = 0.0;
			state = CalculatorState.INPUT_ENTERED;
			operation = ADD_OPERATION;
			numberStack.clear();
			panel.updateUI();
		});
		panel.add(res, "2,7");
		
		JButton equals = new JButton("=");
		equals.addActionListener((evt) -> {
			if (state == CalculatorState.INPUT_ENTERED) {
				labelValue = operation.getResult(storedValue, labelValue);
				if (labelValue != null && Double.isFinite(labelValue)) {
					storedValue = 0.0;
					resultLabel.setText(getTextResult());
					operation = ADD_OPERATION;
					state = CalculatorState.INPUT_ENTERED;
				} else {
					setInvalidInput();
				}
			} else {
				setInvalidInput();
			}
			panel.updateUI();
		});
		panel.add(equals, "1,6");
		
		JButton div = new JButton("/");
		div.addActionListener(new OperationActionListener(DIV_OPERATION));
		panel.add(div, "2,6");
		
		JButton mul = new JButton("*");
		mul.addActionListener(new OperationActionListener(MUL_OPERATION));
		panel.add(mul, "3,6");
		
		JButton sub = new JButton("-");
		sub.addActionListener(new OperationActionListener(SUB_OPERATION));
		panel.add(sub, "4,6");
		
		JButton add = new JButton("+");
		add.addActionListener(new OperationActionListener(ADD_OPERATION));
		panel.add(add, "5,6");
	}
	
	/**
	 * OperationActionListener is {@link ActionListener} for basic operation
	 * buttons.
	 * 
	 * @author Josip Kasap
	 *
	 */
	private class OperationActionListener implements ActionListener {
		
		/** Basic operation strategy. */
		private IBasicOperation oper;
		
		/**
		 * Constructs new OperationActionListener with given basic operation strategy.
		 * @param oper basic operation strategy
		 */
		private OperationActionListener(IBasicOperation oper) {
			this.oper = oper;
		}
		
		@Override
		public void actionPerformed(ActionEvent e) {
			if (state != CalculatorState.INPUT_ENTERED) {
				setInvalidInput();
			} else {
				labelValue = operation.getResult(storedValue, labelValue);
				if (labelValue != null && Double.isFinite(labelValue)) {
					storedValue = labelValue;
					operation = oper;
					resultLabel.setText(getTextResult());
					state = CalculatorState.OPERATION_ENTERED;
				} else {
					setInvalidInput();
				}
			}
			panel.updateUI();
		}
	}
	
	/**
	 * Returns string representation of labelValue.
	 * @return string representation of labelValue
	 */
	private String getTextResult() {
		if (labelValue == null || !Double.isFinite(labelValue)) {
			state = CalculatorState.INVALID_INPUT;
			storedValue = 0.0;
			labelValue = null;
			operation = null;
			return "Invalid input!";
		}
		int intLabelValue = (int)labelValue.doubleValue();
		if (Math.abs(intLabelValue - labelValue) < 1E-6) {
			labelValue = (double)intLabelValue;
			return "" + intLabelValue;
		}
		return labelValue.toString();
	}
	
	/**
	 * Sets everything that needs to be set, so that
	 * calculator would be in invalid input.
	 * Message to be rendered on display label of
	 * calculator is set to "Invalid input!".
	 */
	private void setInvalidInput() {
		setInvalidInput("Invalid input!");
	}
	
	/**
	 * Sets everything that needs to be set, so that
	 * calculator would be in invalid input.
	 * @param message message that is to be rendered on
	 * display label of calculator.
	 */
	private void setInvalidInput(String message) {
		resultLabel.setText(message);
		state = CalculatorState.INVALID_INPUT;
		storedValue = 0.0;
		labelValue = null;
		operation = null;
	}

	/**
	 * Initialized stack buttons of calculator.
	 */
	private void initStackButtions() {
		JButton push = new JButton("push");
		push.addActionListener((evt) -> {
			if (labelValue != null) {
				numberStack.push(labelValue);
			} else {
				setInvalidInput();
				panel.updateUI();
			}
		});
		panel.add(push, "3,7");
		
		JButton pop = new JButton("pop");
		pop.addActionListener((evt) -> {
			if (state == CalculatorState.INVALID_INPUT) {
				setInvalidInput();
			} else if (!numberStack.isEmpty()) {
				labelValue = numberStack.pop();
				resultLabel.setText(getTextResult());
				state = CalculatorState.INPUT_ENTERED;
			} else {
				setInvalidInput("Stack is Empty!");
			}
			panel.updateUI();
		});
		panel.add(pop, "4,7");
	}

	/**
	 * Main method for executing {@link Calculator} program.
	 * @param args command-line arguments (unused).
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(() -> {
			new Calculator().setVisible(true);
		});
	}
}
