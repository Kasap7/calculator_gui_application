package hr.fer.zemris.java.gui.calc;

/**
 * This functional interface represents strategy that describes some basic
 * operations between 2 values such as +, *, -, /, ...
 * 
 * @author Josip Kasap
 *
 */
public interface IBasicOperation {
	
	/**
	 * Method that gets result from operation.
	 * @param firstOperand first operand
	 * @param secondOperand second operand
	 * @return result from operation
	 */
	double getResult(double firstOperand, double secondOperand);
}
