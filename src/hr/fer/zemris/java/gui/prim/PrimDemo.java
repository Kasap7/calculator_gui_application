package hr.fer.zemris.java.gui.prim;

import java.awt.BorderLayout;
import java.awt.event.HierarchyBoundsListener;
import java.awt.event.HierarchyEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

/**
 * PrimDemo is simple program that creates frame with 2 lists of prime numbers
 * and 1 button which adds next number into that list.
 * 
 * @author Josip Kasap
 *
 */
public class PrimDemo extends JFrame {
	
	/** Serial number. */
	private static final long serialVersionUID = 42L;
	
	/**
	 * Constructs new frame with 2 lists of prime numbers
	 * and 1 button which adds next number into that list.
	 */
	public PrimDemo() {
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setTitle("Prim-list");
		setLocation(20, 20);
		setSize(500, 300);
		initGUI();
	}

	
	/**
	 * Initializes gui components.
	 */
	private void initGUI() {
		PrimListModel primListModel = new PrimListModel();
		JList<Integer> list1 = new JList<>(primListModel);
		JList<Integer> list2 = new JList<>(primListModel);
		list1.setLayoutOrientation(JList.VERTICAL);
		list2.setLayoutOrientation(JList.VERTICAL);
		
		JScrollPane listScroller1 = new JScrollPane(list1);
		JScrollPane listScroller2 = new JScrollPane(list2);
		
		JButton next = new JButton("Slijedeći");
		
		JPanel panel = new JPanel(new BorderLayout());
		add(panel);
		
		
		JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
		splitPane.add(listScroller1);
		splitPane.add(listScroller2);
		
		panel.add(next, BorderLayout.SOUTH);
		panel.add(splitPane, BorderLayout.CENTER);
		
		next.addActionListener((evt) -> {
			primListModel.next();
			list1.updateUI();
			list2.updateUI();
		});
		
		splitPane.addHierarchyBoundsListener(new HierarchyBoundsListener(){

			@Override
			public void ancestorMoved(HierarchyEvent arg0) {}

			@Override
			public void ancestorResized(HierarchyEvent arg0) {
				splitPane.setDividerLocation(0.5);
			}
		});
	}

	/**
	 * Main method for executing {@link PrimDemo} program.
	 * @param args command-line arguments (unused)
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(() -> new PrimDemo().setVisible(true));
	}
}
