package hr.fer.zemris.java.gui.prim;

import java.util.ArrayList;
import java.util.List;

import javax.swing.ListModel;
import javax.swing.event.ListDataListener;

/**
 * PrimListModel is implementation of {@link ListModel} which stores prime numbers.
 * When initialized this model will store only number 1 as only prime number. Each
 * call of method {@link #next()} will add next prime number into this model.
 * 
 * @author Josip Kasap
 *
 */
public class PrimListModel implements ListModel<Integer> {
	
	/** List of data listeners. */
	private List<ListDataListener> listeners;
	
	/** List of prime numbers. */
	private List<Integer> elements;
	
	/**
	 * Constructs new PrimListModel and initializes internally stored
	 * list of prime numbers, sets 1 as only prime number
	 * of list.
	 */
	public PrimListModel() {
		super();
		elements = new ArrayList<>();
		listeners = new ArrayList<>();
		elements.add(1);
	}

	@Override
	public void addListDataListener(ListDataListener listener) {
		if (listener == null) {
			throw new IllegalArgumentException("Listeners cannot be null value.");
		}
		if (listeners.contains(listener)) {
			throw new IllegalArgumentException("This model alread contains given listener.");
		}
		listeners.add(listener);
	}

	@Override
	public Integer getElementAt(int index) {
		if (index < 0 || index >= elements.size()) {
			throw new IndexOutOfBoundsException("Index: " + index + " must be in range: [0, " + 
					(elements.size()-1) + "].");
		}
		return elements.get(index);
	}

	@Override
	public int getSize() {
		return elements.size();
	}

	@Override
	public void removeListDataListener(ListDataListener listener) {
		if (listener == null) {
			throw new IllegalArgumentException("Listeners cannot be null value.");
		}
		listeners.remove(listener);
	}
	
	/**
	 * Sets next prime number into list of prime numbers.
	 */
	public void next() {
		int previous = elements.get(elements.size() - 1);
		
		if (previous == 1) {
			elements.add(2);
			return;
		}
		if (previous == 2) {
			elements.add(3);
			return;
		}
		
		while (true) {
			previous += 2;
			boolean isPrime = true;
			for (int i = 2, limit = (int)Math.sqrt(previous); i <= limit; i++) {
				if (previous % i == 0) {
					isPrime = false;
					break;
				}
			}
			if (isPrime) {
				elements.add(previous);
				return;
			}
		}
	}
}
