package hr.fer.zemris.java.gui.charts;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;

import javax.swing.JComponent;

/**
 * BarChartComponent is {@link JComponent} that represents some chart.
 * This component has only picture of chart, which is rendered through
 * its entirety if it fits. This component has no other functions than
 * showing image of chart that it possesses.
 * 
 * @author Josip Kasap
 *
 */
public class BarChartComponent extends JComponent {
	
	/** Serial number. */
	private static final long serialVersionUID = 42L;
	
	/** Spacing between 2 chart boxes. */
	private static final int INTERSPACE = 5;
	
	/** Minimal width of chart box. */
	private static final int MIN_CHART_WIDTH = 5;
	
	/**	Minimal height difference between two y values. */
	private static final int MIN_CHART_HEIGHT_DIFFERENCE = 5;
	
	/** Distance from string representation of axes to number values of that axes. */
	private static final int DISTANCE_STRING_TO_NUMBERS = 15;
	
	/** Distance from values of axes to chart. */
	private static final int DISTANCE_NUMBERS_TO_CHART = 10;
	
	/** Distance from string representation of axes to borders of this component. */
	private static final int DISTANCE_BORDER_TO_STRING = 5;
	
	/** Distance from borders of component to chart. */
	private static final int DISTANCE_BORDER_TO_CHART = 5;
	
	/** Distance of mini-lines that define number values of axes. */
	private static final int LINE_CROSSING_DISTANCE = 3;
	
	/** Length of arrows at the end of the axes. */
	private static final int ARROW_LENGTH = 4;
	
	/** Chart that is to be rendered in this component. */
	private BarChart chart;
	
	/**
	 * Constructs new BarChartComponent from given {@link BarChart}.
	 * @param chart {@link BarChart} that is to be rendered in this component
	 */
	public BarChartComponent(BarChart chart) {
		if (chart == null) {
			throw new IllegalArgumentException("Chart cannot be null value.");
		}
		
		this.chart = chart;
	}

	@Override
	public void paint(Graphics g) {
		g.setFont(new Font("Arial", Font.ROMAN_BASELINE, 16));

		Dimension size = getMaximumDimension(getSize(), minDimension(g));
		Graphics2D g2d = (Graphics2D) g;
		
		AffineTransform defaultAt = g2d.getTransform();
		AffineTransform at = new AffineTransform();
		
		FontMetrics fm = g.getFontMetrics();
		int heightY = fm.stringWidth(chart.getYDescription());
		int widthX = fm.stringWidth(chart.getXDescription());
		int ascent = fm.getAscent();
		int maxYwidth = getMaximumYnumberWidth(g);
		
		int positionY = (size.width - widthX)/2;
		
		g2d.drawString(chart.getXDescription(), positionY, size.height - (DISTANCE_BORDER_TO_STRING + ascent));
		
		at.rotate(- Math.PI / 2);
		g2d.setTransform(at);
		
		int positionX = -(size.height + heightY)/2;
		
		g2d.drawString(chart.getYDescription(), positionX, DISTANCE_BORDER_TO_STRING + ascent);
		g2d.setTransform(defaultAt);
		
		int chartWidth = size.width - ascent*2 - DISTANCE_BORDER_TO_STRING - DISTANCE_STRING_TO_NUMBERS
				- DISTANCE_NUMBERS_TO_CHART - DISTANCE_BORDER_TO_CHART - maxYwidth;
		
		int chartHeight = size.height - ascent*3 - DISTANCE_BORDER_TO_STRING - DISTANCE_STRING_TO_NUMBERS
				- DISTANCE_NUMBERS_TO_CHART - DISTANCE_BORDER_TO_CHART;
		
		int rowWidth = ( chartWidth - (chart.getXYSet().size() - 1)*INTERSPACE ) / chart.getXYSet().size();
		int rowHeight = ( chartHeight - ((chart.getYmax() - chart.getYmin())/chart.getDifference() - 1)
				*INTERSPACE ) / ((chart.getYmax() - chart.getYmin() - 1)/chart.getDifference());
		
		int i = 0;
		
		for (int j=chart.getYmin(), max=chart.getYmax(), dif=chart.getDifference(); j <= max; j += dif, i++) {
			g2d.setColor(Color.BLACK);
			int numberWidth = fm.stringWidth("" + j);
			int distance = maxYwidth - numberWidth;
			g2d.drawString(j + "",
					DISTANCE_BORDER_TO_STRING + ascent*2 + DISTANCE_STRING_TO_NUMBERS + distance, 
					size.height - DISTANCE_BORDER_TO_STRING - ascent*3 - DISTANCE_STRING_TO_NUMBERS
					- DISTANCE_NUMBERS_TO_CHART - rowHeight*i
			);
			
			g2d.setColor(Color.LIGHT_GRAY);
			g2d.drawLine(
					DISTANCE_BORDER_TO_STRING + ascent*2 + DISTANCE_STRING_TO_NUMBERS + maxYwidth
					+ DISTANCE_NUMBERS_TO_CHART - LINE_CROSSING_DISTANCE, 
					
					size.height - DISTANCE_BORDER_TO_STRING - ascent*3 - DISTANCE_STRING_TO_NUMBERS
					- DISTANCE_NUMBERS_TO_CHART - ascent/2 - i*rowHeight, 
					
					size.width - DISTANCE_BORDER_TO_CHART, 
					
					size.height - DISTANCE_BORDER_TO_STRING - ascent*3 - DISTANCE_STRING_TO_NUMBERS
					- DISTANCE_NUMBERS_TO_CHART - ascent/2 - i*rowHeight
			);
		}
		
		i = 0;
		for (XYValue xyValue : chart.getXYSet()) {
			int numberWidth = fm.stringWidth("" + xyValue.getX());
			g2d.setColor(Color.BLACK);
			g2d.drawString(xyValue.getX() + "",
					DISTANCE_BORDER_TO_STRING + ascent*2 + DISTANCE_STRING_TO_NUMBERS + maxYwidth
					+ DISTANCE_NUMBERS_TO_CHART + i*rowWidth + (rowWidth - numberWidth) / 2, 
					size.height - DISTANCE_BORDER_TO_STRING - ascent*2 - DISTANCE_STRING_TO_NUMBERS
			);
			
			i++;
			g2d.setColor(Color.LIGHT_GRAY);
			g2d.drawLine(
					DISTANCE_BORDER_TO_STRING + ascent*2 + DISTANCE_STRING_TO_NUMBERS + maxYwidth
					+ DISTANCE_NUMBERS_TO_CHART + i*rowWidth, 
					
					size.height - DISTANCE_BORDER_TO_STRING - ascent*3 - DISTANCE_STRING_TO_NUMBERS
					- DISTANCE_NUMBERS_TO_CHART + LINE_CROSSING_DISTANCE - ascent/2, 
					
					DISTANCE_BORDER_TO_STRING + ascent*2 + DISTANCE_STRING_TO_NUMBERS + maxYwidth
					+ DISTANCE_NUMBERS_TO_CHART + i*rowWidth, 
					
					DISTANCE_BORDER_TO_CHART
			);
			
			g2d.setColor(Color.PINK);
			g2d.fillRect(
					DISTANCE_BORDER_TO_STRING + ascent*2 + DISTANCE_STRING_TO_NUMBERS + maxYwidth
					+ DISTANCE_NUMBERS_TO_CHART + (i - 1)*rowWidth, 
					
					size.height - DISTANCE_BORDER_TO_STRING - ascent*3 - DISTANCE_STRING_TO_NUMBERS
					- DISTANCE_NUMBERS_TO_CHART - ascent/2 
					- ((xyValue.getY() / chart.getDifference()) * rowHeight),
					
					rowWidth - INTERSPACE, 
					
					(xyValue.getY() / chart.getDifference()) * rowHeight
			);
			
			g2d.setColor(this.getBackground());
			
			g2d.fillRect(
					DISTANCE_BORDER_TO_STRING + ascent*2 + DISTANCE_STRING_TO_NUMBERS + maxYwidth
					+ DISTANCE_NUMBERS_TO_CHART + i*rowWidth - INTERSPACE, 
					
					size.height - DISTANCE_BORDER_TO_STRING - ascent*3 - DISTANCE_STRING_TO_NUMBERS
					- DISTANCE_NUMBERS_TO_CHART - ascent/2 
					- ((xyValue.getY() / chart.getDifference()) * rowHeight) + 1,
					
					INTERSPACE, 
					
					(xyValue.getY() / chart.getDifference()) * rowHeight
			);
		}
		
		g2d.setColor(Color.BLACK);
		g2d.drawLine(
				DISTANCE_BORDER_TO_STRING + ascent*2 + DISTANCE_STRING_TO_NUMBERS + maxYwidth
				+ DISTANCE_NUMBERS_TO_CHART, 
				
				size.height - DISTANCE_BORDER_TO_STRING - ascent*3 - DISTANCE_STRING_TO_NUMBERS
				- DISTANCE_NUMBERS_TO_CHART + LINE_CROSSING_DISTANCE - ascent/2, 
				
				DISTANCE_BORDER_TO_STRING + ascent*2 + DISTANCE_STRING_TO_NUMBERS + maxYwidth
				+ DISTANCE_NUMBERS_TO_CHART, 
				
				DISTANCE_BORDER_TO_CHART
		);
		g2d.drawLine(
				DISTANCE_BORDER_TO_STRING + ascent*2 + DISTANCE_STRING_TO_NUMBERS + maxYwidth
				+ DISTANCE_NUMBERS_TO_CHART - LINE_CROSSING_DISTANCE, 
				
				size.height - DISTANCE_BORDER_TO_STRING - ascent*3 - DISTANCE_STRING_TO_NUMBERS
				- DISTANCE_NUMBERS_TO_CHART - ascent/2, 
				
				size.width - DISTANCE_BORDER_TO_CHART, 
				
				size.height - DISTANCE_BORDER_TO_STRING - ascent*3 - DISTANCE_STRING_TO_NUMBERS
				- DISTANCE_NUMBERS_TO_CHART - ascent/2
		);
		
		g2d.drawLine(
				DISTANCE_BORDER_TO_STRING + ascent*2 + DISTANCE_STRING_TO_NUMBERS + maxYwidth
				+ DISTANCE_NUMBERS_TO_CHART, 
				
				DISTANCE_BORDER_TO_CHART,
				
				DISTANCE_BORDER_TO_STRING + ascent*2 + DISTANCE_STRING_TO_NUMBERS + maxYwidth
				+ DISTANCE_NUMBERS_TO_CHART - ARROW_LENGTH,
				
				DISTANCE_BORDER_TO_CHART + ARROW_LENGTH
		);
		g2d.drawLine(
				DISTANCE_BORDER_TO_STRING + ascent*2 + DISTANCE_STRING_TO_NUMBERS + maxYwidth
				+ DISTANCE_NUMBERS_TO_CHART, 
				
				DISTANCE_BORDER_TO_CHART,
				
				DISTANCE_BORDER_TO_STRING + ascent*2 + DISTANCE_STRING_TO_NUMBERS + maxYwidth
				+ DISTANCE_NUMBERS_TO_CHART + ARROW_LENGTH,
				
				DISTANCE_BORDER_TO_CHART + ARROW_LENGTH
		);
		
		g2d.drawLine(
				size.width - DISTANCE_BORDER_TO_CHART, 
				
				size.height - DISTANCE_BORDER_TO_STRING - ascent*3 - DISTANCE_STRING_TO_NUMBERS
				- DISTANCE_NUMBERS_TO_CHART - ascent/2,
				
				size.width - DISTANCE_BORDER_TO_CHART - ARROW_LENGTH,
				
				size.height - DISTANCE_BORDER_TO_STRING - ascent*3 - DISTANCE_STRING_TO_NUMBERS
				- DISTANCE_NUMBERS_TO_CHART - ascent/2 - ARROW_LENGTH
		);
		g2d.drawLine(
				size.width - DISTANCE_BORDER_TO_CHART, 
				
				size.height - DISTANCE_BORDER_TO_STRING - ascent*3 - DISTANCE_STRING_TO_NUMBERS
				- DISTANCE_NUMBERS_TO_CHART - ascent/2,
				
				size.width - DISTANCE_BORDER_TO_CHART - ARROW_LENGTH,
				
				size.height - DISTANCE_BORDER_TO_STRING - ascent*3 - DISTANCE_STRING_TO_NUMBERS
				- DISTANCE_NUMBERS_TO_CHART - ascent/2 + ARROW_LENGTH
		);
	}
	
	/**
	 * Returns minimal dimension that this component can render full image of.
	 * If size of component is lower that this value, component will not render whole
	 * image.
	 * @param g graphics used to obtain distance from objects that are to be rendered
	 * @return minimal {@link Dimension} that this component can render full image of.
	 */
	private Dimension minDimension(Graphics g) {
		FontMetrics fm = g.getFontMetrics();
		
	    int heightY = fm.stringWidth(chart.getYDescription());
	    int ascent = fm.getAscent();
	    int widthX = fm.stringWidth(chart.getXDescription());

	    int numOfX = chart.getXYSet().size();
	    int numOfY = (chart.getYmax() - chart.getYmin())/chart.getDifference();
	    
	    int chartWidth = Math.max(MIN_CHART_WIDTH, getMaximumXnumberWIdth(g) + 2);
	    int chartHeight = Math.max(MIN_CHART_HEIGHT_DIFFERENCE, ascent + 2);
	    
	    int minWidth = Math.max(
	    		 widthX, 
	    		 numOfX*(chartWidth + INTERSPACE) - INTERSPACE + DISTANCE_NUMBERS_TO_CHART 
	    		 + DISTANCE_STRING_TO_NUMBERS + ascent*3 + getMaximumYnumberWidth(g)
	    		 + DISTANCE_BORDER_TO_STRING + DISTANCE_BORDER_TO_CHART
	    );
	    
	    int minHeight = Math.max(
	    		heightY, 
	    		numOfY*(chartHeight + INTERSPACE) - INTERSPACE + DISTANCE_NUMBERS_TO_CHART 
	    		 + DISTANCE_STRING_TO_NUMBERS + ascent*2 + DISTANCE_BORDER_TO_STRING
	    );
	    
	    return new Dimension(minWidth, minHeight);
	}
	
	/**
	 * Helper method that returns maximal x axes number value width.
	 * @param g graphics used to obtain maximal x axes number value width
	 * @return maximal x axes number value width
	 */
	private int getMaximumXnumberWIdth(Graphics g) {
		FontMetrics fm = g.getFontMetrics();
		return fm.stringWidth( chart.getXYSet().toArray(new XYValue[chart.getXYSet()
		        .size()])[chart.getXYSet().size()-1].getX() + "");
	}
	
	/**
	 * Helper method that returns maximal y axes number value width.
	 * @param g graphics used to obtain maximal y axes number value width
	 * @return maximal y axes number value width
	 */
	private int getMaximumYnumberWidth(Graphics g) {
		FontMetrics fm = g.getFontMetrics();
		return fm.stringWidth("" + chart.getYmax());
	}
	
	/**
	 * Helper method that returns new Dimension whose properties
	 * are maximum of properties from given 2 dimensions.
	 * @param d1 first dimension
	 * @param d2 second dimension
	 * @return new Dimension whose properties are maximum of 
	 * 			properties from given 2 dimensions
	 */
	private Dimension getMaximumDimension(Dimension d1, Dimension d2) {
		Dimension d = new Dimension();
		d.height = d1.height > d2.height ? d1.height : d2.height;
		d.width = d1.width > d2.width ? d1.width : d2.width;
		return d;
	}
	
}
