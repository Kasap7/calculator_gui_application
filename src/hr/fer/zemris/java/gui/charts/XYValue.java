package hr.fer.zemris.java.gui.charts;

/**
 * XYValue is class which holds 2 properties: integer x value and integer y value.
 * This class has no other purposes, it is only a container for 2 values.
 * 
 * @author Josip Kasap
 *
 */
public class XYValue implements Comparable<XYValue> {

	/** X value. */
	private int x;
	
	/** Y value. */
	private int y;
	
	/**
	 * Constructs new XYValue with given x and y values.
	 * @param x x value
	 * @param y y value
	 */
	public XYValue(int x, int y) {
		super();
		this.x = x;
		this.y = y;
	}
	
	/**
	 * Getter for x value
	 * @return x value
	 */
	public int getX() {
		return x;
	}
	
	/**
	 * Setter for x value.
	 * @param x new x value
	 */
	public void setX(int x) {
		this.x = x;
	}
	
	/**
	 * Getter for y value
	 * @return y value
	 */
	public int getY() {
		return y;
	}
	
	/**
	 * Setter for y value.
	 * @param y new y value
	 */
	public void setY(int y) {
		this.y = y;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + x;
		result = prime * result + y;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		XYValue other = (XYValue) obj;
		if (x != other.x)
			return false;
		if (y != other.y)
			return false;
		return true;
	}

	@Override
	public int compareTo(XYValue other) {
		return this.x - other.x;
	}
	
	/**
	 * Parses string into XYvalue.
	 * String has to be in format: x,\\s*y; where x and y are x and y values.
	 * @param s string representation of XYValue
	 * @return new XYValue that represents XYValue given by string
	 * @throws IllegalArgumentException if string is null value
	 * @throws IllegalArgumentException if string does not represent XYValue
	 */
	public static XYValue parse(String s) {
		if (s == null) {
			throw new IllegalArgumentException("Parsing String cannot be null value.");
		}
		s = s.trim();
		try {
			String[] splited = s.split(",\\s*");
			if (splited.length != 2) {
				throw new IllegalArgumentException("Given String does not represent valid XYValue.");
			}
			
			return new XYValue(Integer.parseInt(splited[0]), Integer.parseInt(splited[1]));
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException("Given String does not represent valid XYValue.");
		}
	}
}
