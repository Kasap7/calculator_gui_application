package hr.fer.zemris.java.gui.charts;

import java.awt.BorderLayout;
import java.io.BufferedReader;
import java.io.FileReader;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

/**
 * BarChartDemo is program that shows simple frame with {@link BarChartComponent}
 * as its main component. This programs only purpose is to display that chart
 * thus showing its design and quality. This program expects single argument:
 * Path to file that contains some chart information.
 * 
 * @author Josip Kasap
 *
 */
public class BarChartDemo extends JFrame {
	
	/** Serial number. */
	private static final long serialVersionUID = 42L;
	
	/**
	 * Constructs new Chart frame with given file name and {@link BarChart}.
	 * @param fileName name of the file from which chart has been successfully extracted.
	 * @param chart chart that is to be rendered in this frame.
	 */
	public BarChartDemo(String fileName, BarChart chart) {
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setTitle("Chart");
		setLocation(20, 20);
		setSize(800, 600);
		initGUI(fileName, chart);
	}

	/**
	 * Initializes gui.
	 * @param fileName name of the file from which chart has been successfully extracted.
	 * @param chart chart that is to be rendered in this frame.
	 */
	private void initGUI(String fileName, BarChart chart) {
		JPanel panel = new JPanel(new BorderLayout());
		add(panel);
		JLabel label = new JLabel(fileName);
		label.setHorizontalAlignment(SwingConstants.CENTER);
		panel.add(label, BorderLayout.NORTH);
		panel.add(new BarChartComponent(chart), BorderLayout.CENTER);
	}

	/**
	 * Main method for executing the {@link BarChartDemo} program.
	 * @param args command-line arguments
	 */
	public static void main(String[] args) {
		
		if (args.length != 1) {
			System.err.println("There must be exactly 1 command line argument:\n"
					+ "path file to file with BarChart description.");
			System.exit(0);
		}
		
		BarChart chart = getBarChartFromFile(args[0]);
		if (chart == null) {
			System.exit(0);
		}
		
		SwingUtilities.invokeLater(() -> {
			new BarChartDemo(Paths.get(args[0]).toAbsolutePath().toString(), chart).setVisible(true);
		});
	}

	/**
	 * Helper method for extracting {@link BarChart} from file.
	 * @param fileName file name of file that contains some chart
	 * @return chart if file contains chart, or null othervise
	 */
	private static BarChart getBarChartFromFile(String fileName) {
		try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
			
			String xDescription = reader.readLine();
			String yDescription = reader.readLine();
			List<XYValue> xyList = new ArrayList<>();
			String[] xyValues = reader.readLine().split("\\s+");
			
			for (String xyString : xyValues) {
				xyList.add(XYValue.parse(xyString));
			}
			
			int ymin = Integer.parseInt(reader.readLine());
			int ymax = Integer.parseInt(reader.readLine());
			int ydiff = Integer.parseInt(reader.readLine());
			
			return new BarChart(
					xyList, xDescription, yDescription, ymin, ymax, ydiff
			);
		} catch (Exception e) {
			System.err.println("Exception happened, message: " + e.getMessage());
			return null;
		}
	}
}
