package hr.fer.zemris.java.gui.charts;

import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * BarChart represents some chart. BarChart gives description of
 * x and y axes, and set of values representing that chart.
 * 
 * @author Josip Kasap
 *
 */
public class BarChart {

	/** Set of x - y values that represents one box in chart. */
	private Set<XYValue> xySet;
	
	/** Description of x axes. */
	private String xDescription;
	
	/** Description of y axes. */
	private String yDescription;
	
	/** Minimal y value of chart. */
	private int ymin;
	
	/** Minimal x value of chart. */
	private int ymax;
	
	/** Difference between 2 y values in y axes representation. */
	private int ydiff;
	
	/**
	 * Constructs new BarChart with given description of BarChart.
	 * @param xyList list of XYValues of chart
	 * @param xDescription description of x axes
	 * @param yDescription description of y axes
	 * @param ymin minimal y value
	 * @param ymax maximal y value
	 * @param ydiff difference between 2 y values in y axes representation
	 */
	public BarChart(List<XYValue> xyList, String xDescription, String yDescription,
			int ymin, int ymax, int ydiff) {
		super();
		if(xyList == null || xDescription == null || yDescription == null) {
			throw new IllegalArgumentException("Null value is not allowed for this constructor.");
		}
		
		if (ydiff <= 0) {
			throw new IllegalArgumentException("Difference must be greater than 0.");
		}
		
		if (ymax <= ymin) {
			throw new IllegalArgumentException("Ymax must be greater than Ymin.");
		}
		
		if (ymax - ymin < ydiff) {
			throw new IllegalArgumentException("Given difference must be smaller than given range of values.");
		}
		
		xySet = new TreeSet<>();
		xySet.addAll(xyList);
		this.xDescription = xDescription;
		this.yDescription = yDescription;
		
		this.ydiff = ydiff;
		this.ymin = ymin;
		if ((ymax - ymin) % ydiff == 0) {
			this.ymax = ymax;
		} else {
			this.ymax = ymin + ((ymax - ymin)/ydiff + 1)*ydiff;
		}
	}

	/**
	 * Getter of set of {@link XYValue}
	 * @return set of {@link XYValue}
	 */
	public Set<XYValue> getXYSet() {
		return xySet;
	}

	/**
	 * Getter for description of x axes.
	 * @return description of x axes
	 */
	public String getXDescription() {
		return xDescription;
	}

	/**
	 * Getter for description of y axes.
	 * @return description of y axes
	 */
	public String getYDescription() {
		return yDescription;
	}

	/**
	 * Getter for minimal y value.
	 * @return minimal y value
	 */
	public int getYmin() {
		return ymin;
	}

	/**
	 * Getter for maximal y value.
	 * @return maximal y value
	 */
	public int getYmax() {
		return ymax;
	}

	/**
	 * Getter for y difference value.
	 * @return y difference value
	 */
	public int getDifference() {
		return ydiff;
	}
}
