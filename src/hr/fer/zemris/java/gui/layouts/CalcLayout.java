package hr.fer.zemris.java.gui.layouts;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.LayoutManager2;
import java.util.HashMap;
import java.util.Map;

/**
 * CalcLayout is implementation of {@link LayoutManager2} which makes calculator
 * appearance layout. This layout is suitable for calculator frames or some which
 * are very similar in component placements.
 * <p> Valid positions of components are anywhere in table [7, 5];
 * 
 * @author Josip Kasap
 *
 */
public class CalcLayout implements LayoutManager2 {
	
	/** Number of pixels that separates each component from its neighbor */
	private int interspace;
	
	/** Maps position with its component. */
	private Map<RCPosition, Component> positionMap;
	
	/** Maximal column position. */
	private static final int MAX_COLUMN_NUMBER = 7;
	
	/** Maximal row position. */
	private static final int MAX_ROW_NUMBER = 5;
	
	/** Number of regular positions in same row that first component (1,1) uses. */
	private static final int FIRST_POSITION_NUMBER = 5;
	
	/** Strategy for getting minimal size from component. */
	private static final ISizeGetter MIN_SIZE_GETTER = (comp) -> comp.getMinimumSize();
	
	/** Strategy for getting maximal size from component. */
	private static final ISizeGetter MAX_SIZE_GETTER = (comp) -> comp.getMaximumSize();
	
	/** Strategy for getting preferred size from component. */
	private static final ISizeGetter PREFERRED_SIZE_GETTER = (comp) -> comp.getPreferredSize();
	
	/**
	 * Default constructor which constructs new CalcLayout with no spacing
	 * between neighboring components.
	 */
	public CalcLayout() {
		this(0);
	}
	
	/**
	 * Constructs new CalcLayout with given spacing between neighboring components.
	 * @param interspace spacing between neighboring components
	 * @throws IllegalArgumentException if spacing between neighboring component is lower than 0
	 */
	public CalcLayout(int interspace) {
		super();
		if (interspace < 0) {
			throw new IllegalArgumentException("Spacing between components must be greater than 0!");
		}
		this.interspace = interspace;
		positionMap = new HashMap<>();
	}

	@Override
	public void addLayoutComponent(String rcPos, Component comp) {
		addLayoutComponent(comp, RCPosition.parse(rcPos));
	}

	@Override
	public void layoutContainer(Container parent) {
		if (parent == null) {
			throw new IllegalArgumentException("Container cannot be null value.");
		}
		
		Dimension parentDim = getMaximumDimension(parent.getSize(), minimumLayoutSize(parent));
		parent.setSize(parentDim);
		
		int borderWidth, borderHeigth;
		Insets ins = parent.getInsets();
		
		borderWidth = ins.left + ins.right;
		borderHeigth = ins.top + ins.bottom;
		
		int maxWidth = (parentDim.width - borderWidth - interspace*(MAX_COLUMN_NUMBER-1))
				/MAX_COLUMN_NUMBER;
		int maxHeigth = (parentDim.height - borderHeigth - interspace*(MAX_ROW_NUMBER-1))
				/MAX_ROW_NUMBER;
		
		for (Map.Entry<RCPosition, Component> entry : positionMap.entrySet()) {
			Component comp = entry.getValue();
			RCPosition position = entry.getKey();

			int xPosition = ins.left + (maxWidth + interspace) * (position.getColumn() - 1);
			int yPosition = ins.top + (maxHeigth + interspace) * (position.getRow() - 1);
			comp.setLocation(xPosition, yPosition);
			
			if (position.getRow() == 1 && position.getColumn() == 1) {
				comp.setSize(maxWidth*FIRST_POSITION_NUMBER + interspace*(FIRST_POSITION_NUMBER - 1),
						maxHeigth
				);
			} else {
				comp.setSize(maxWidth, maxHeigth);
			}
		}
	}
	
	/**
	 * Method which returns new dimension with maximal values
	 * of properties from given dimensions.
	 * @param d1 first dimension
	 * @param d2 second dimension
	 * @return new dimension with maximal values of properties from given dimensions
	 */
	private static Dimension getMaximumDimension(Dimension d1, Dimension d2) {
		Dimension d = new Dimension();
		if (d1.height > d2.height) {
			d.height = d1.height;
		} else {
			d.height = d2.height;
		}
		
		if (d1.width > d2.width) {
			d.width = d1.width;
		} else {
			d.width = d2.width;
		}
		return d;
	}

	@Override
	public Dimension minimumLayoutSize(Container target) {
		return getLayoutSize(target, MIN_SIZE_GETTER);
	}

	@Override
	public Dimension preferredLayoutSize(Container target) {
		return getLayoutSize(target, PREFERRED_SIZE_GETTER);
	}
	
	/**
	 * Method which gets layout size of container.
	 * @param container parent Container
	 * @param sizeGetter Strategy for obtaining Dimension from component
	 * @return Dimension of whole container
	 * @throws IllegalArgumentException if container is null value
	 */
	private Dimension getLayoutSize(Container container, ISizeGetter sizeGetter) {
		if (container == null) {
			throw new IllegalArgumentException("Container cannot be null value.");
		}
		
		int borderWidth, borderHeigth;
		Insets ins = container.getInsets();
		
		borderWidth = ins.left + ins.right;
		borderHeigth = ins.top + ins.bottom;
		
		int maxWidth = 0, maxHeigth = 0, firstElementWidth = 0;
		for (Map.Entry<RCPosition, Component> entry : positionMap.entrySet()) {
			RCPosition pos = entry.getKey();
			Dimension dim = sizeGetter.getSize(entry.getValue());
			
			if (dim == null) continue;
			
			if (dim.height > maxHeigth) {
				maxHeigth = dim.height;
			}
			
			if (pos.getRow() == 1 && pos.getColumn() == 1) {
				firstElementWidth = dim.width;
				continue;
			}
			
			if (dim.width > maxWidth) {
				maxWidth = dim.width;
			}
		}
		
		maxWidth = getMaximumWidth(maxWidth, firstElementWidth);
		
		Dimension size = new Dimension(
				borderWidth + maxWidth*MAX_COLUMN_NUMBER + interspace*(MAX_COLUMN_NUMBER - 1), 
				borderHeigth + maxHeigth*MAX_ROW_NUMBER + interspace*(MAX_ROW_NUMBER - 1)
		);
		
		return size;
	}
	
	
	/**
	 * Helping method that determines if first component has larger width than
	 * 5 times the amount of maximal width of other components with spacing between
	 * them.
	 * @param maxWidth maximal width of other components
	 * @param firstElementWidth width of first component
	 * @return greater value between 2 presented cases
	 */
	private int getMaximumWidth(int maxWidth, int firstElementWidth) {
		if (firstElementWidth < maxWidth*(FIRST_POSITION_NUMBER) 
				+ (FIRST_POSITION_NUMBER - 1)*interspace) {
			
			return maxWidth;
		}
		
		maxWidth = (firstElementWidth - (FIRST_POSITION_NUMBER - 1)*interspace)/FIRST_POSITION_NUMBER;
		if ((firstElementWidth - (FIRST_POSITION_NUMBER - 1)*interspace) % FIRST_POSITION_NUMBER != 0) {
			maxWidth++;
		}
		
		return maxWidth;
	}

	@Override
	public void removeLayoutComponent(Component comp) {
		if (comp == null) {
			throw new IllegalArgumentException("Component cannot be null value.");
		}
		
		RCPosition removed = null;
		for (Map.Entry<RCPosition, Component> entry : positionMap.entrySet()) {
			if (entry.getValue().equals(comp)) {
				removed = entry.getKey();
				break;
			}
		}
		
		if (removed != null) {
			positionMap.remove(removed);
		}
	}

	@Override
	public void addLayoutComponent(Component comp, Object constraints) {
		if (comp == null) {
			throw new IllegalArgumentException("Component cannot be null value.");
		}
		
		if (constraints == null) {
			throw new IllegalArgumentException("This layout rquires constraints, and they must be \n"
					+ "instances of RCPosition, or string representation of that class.");
		}
		
		if (constraints instanceof String) {
			constraints = RCPosition.parse((String)constraints);
		} else if (!(constraints instanceof RCPosition)) {
			throw new IllegalArgumentException("Constraints must be instance of RCPosition.");
		}
		
		RCPosition rcPos = (RCPosition)constraints;
		if (positionMap.get(rcPos) != null) {
			throw new IllegalArgumentException("This Layout already contains component on that constraint.");
		}
		
		validatePosition(rcPos);
		positionMap.put(rcPos, comp);
	}
	
	/**
	 * Method that validates if given {@link RCPosition} is valid position
	 * for this layout.
	 * @param position position of some component
	 * @throws IllegalArgumentException if given position is not valid position for
	 * 			CalcLayout
	 */
	private void validatePosition(RCPosition position) {
		if (position.getColumn() > MAX_COLUMN_NUMBER || position.getRow() > MAX_ROW_NUMBER) {
			throw new IllegalArgumentException("Invalid position: " + position + "; valid position must be "
					+ "in range: [" + MAX_ROW_NUMBER + "," + MAX_COLUMN_NUMBER + "]"
			);
		} else if (position.getRow() == 1) {
			if (position.getColumn() != 1 && position.getColumn() <= FIRST_POSITION_NUMBER) {
				throw new IllegalArgumentException("Invalid position: " + position + "; on first row "
						+ "only 1 or number greater than " + FIRST_POSITION_NUMBER + " is allowed."
				);
			}
		}
	}

	@Override
	public float getLayoutAlignmentX(Container target) {
		return 0.5F;
	}

	@Override
	public float getLayoutAlignmentY(Container target) {
		return 0.5F;
	}

	@Override
	public void invalidateLayout(Container target) {
	}

	@Override
	public Dimension maximumLayoutSize(Container target) {
		return getLayoutSize(target, MAX_SIZE_GETTER);
	}
	
	/**
	 * Strategy for obtaining Dimension from component
	 * 
	 * @author Josip Kasap
	 *
	 */
	private interface ISizeGetter {
		
		/**
		 * Gets dimension of component.
		 * @param comp Component
		 * @return dimension of component
		 */
		Dimension getSize(Component comp);
	}
}
