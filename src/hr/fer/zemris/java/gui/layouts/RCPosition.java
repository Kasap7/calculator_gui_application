package hr.fer.zemris.java.gui.layouts;

/**
 * RCPosition is used to represent position in some table. RCPosition is class 
 * which holds two integer properties. Row property represents row number 
 * of some component. Column property represents column number of some component.
 * 
 * @author Josip Kasap
 *
 */
public class RCPosition {

	/** Row position. */
	private int row;
	
	/** Column position. */
	private int column;
	
	/**
	 * Constructs new RCPosition with given row and column position.
	 * @param row row position
	 * @param column column position
	 * @throws IllegalArgumentException if row or columnt position is lower or equal than 0
	 */
	public RCPosition(int row, int column) {
		super();
		if (row <= 0 || column <= 0) {
			throw new IllegalArgumentException("Position of row and column must be greater than 0.");
		}
		this.row = row;
		this.column = column;
	}
	
	/**
	 * Getter for row property.
	 * @return row position
	 */
	public int getRow() {
		return row;
	}
	
	/**
	 * Setter for row property
	 * @param row row position
	 * @throws IllegalArgumentException if row is lower or equal than 0
	 */
	public void setRow(int row) {
		if (row <= 0){
			throw new IllegalArgumentException("Position of row must be greater than 0.");
		}
		this.row = row;
	}
	
	/**
	 * Getter for column property.
	 * @return column position
	 */
	public int getColumn() {
		return column;
	}
	
	/**
	 * Setter for column property
	 * @param column column position
	 * @throws IllegalArgumentException if column is lower or equal than 0
	 */
	public void setColumn(int column) {
		if (column <= 0){
			throw new IllegalArgumentException("Position of coulmn must be greater than 0.");
		}
		this.column = column;
	}
	
	/**
	 * Parses given string and return RCPosition that this string represents.
	 * @param s string value of RCPosition
	 * @return RCPosition that this string is representing
	 * @throws IllegalArgumentException if string is null value
	 * @throws IllegalArgumentException if string does not represent RCPosition
	 */
	public static RCPosition parse(String s) {
		if (s == null) {
			throw new IllegalArgumentException("String that represents RCPosition cannot be null value.");
		}
		
		s = s.trim();
		String[] rowAndColumn = s.split(",|,\\s+");
		
		try {
			int row = Integer.parseInt(rowAndColumn[0]);
			int column = Integer.parseInt(rowAndColumn[1]);
			return new RCPosition(row, column);
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException("String does not represent valid RCPosition.");
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + column;
		result = prime * result + row;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RCPosition other = (RCPosition) obj;
		if (column != other.column)
			return false;
		if (row != other.row)
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return row + "," + column;
	}
}
